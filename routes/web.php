<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', function () {
     return view('welcome');
 });
Route::group([
	'namespace' => 'Backend'
],function(){
	Route::group([
		'namespace' => 'Admins'
	],function(){
        route::match(['get', 'post'],'home','LoginController@getlogin')->name('admin.login');
        route::post('home','LoginController@postLogin')->name('admin.export');
        route::get('dashboard','AdminController@homeAdmin')->name('home.admin');
        route::any('logout','LoginController@getRegistration')->name('admin.register');
        route::any('Registration','LoginController@postRegistration')->name('admin.postLogin');
        route::any('proType','Product_TypeController@index')->name('proType.admin');
        route::any('destroy{id}','Product_TypeController@destroy')->name('proType.destroy');
        route::any('proDetail','product_DetailController@store')->name('proDetail.add');
        route::any('proView','product_DetailController@view')->name('proDetail.view');
        route::any('delete/{id}','product_DetailController@delete')->name('proDetail.delete');
//        Route::post('register',[
//		 	'as' => 'admin.register',
//		 	'user' => 'AdminController@register'
//		 ]);
	});
});

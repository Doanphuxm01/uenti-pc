<?php

use App\Models\admin;
use App\Models\Product;
use App\Models\Product_Detail;
use App\Models\Product_Type;
use App\Models\roles;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(User::class,10)->create();
        factory(roles::class,2)->create();
        factory(admin::class,10)->create();
        factory(Product_Type::class,5)->create();
        factory(Product_Detail::class,5)->create();
        factory(Product::class,5)->create();

    }
}

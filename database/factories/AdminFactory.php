<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\admin;
use App\Models\roles;
use Faker\Generator as Faker;

$factory->define(admin::class, function (Faker $faker) {
    return [
        'name' => $faker->text,
        'user_name' => $faker->user_name,
        'role_id' => function(){
            return roles::all()->random();
        },
        'address' => $faker->address,
        'phone_number'  => $faker->phone_number,
        'email'  => $faker->email,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',



    ];
});

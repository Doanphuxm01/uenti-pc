<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product_Detail;
use Faker\Generator as Faker;

$factory->define(Product_Detail::class, function (Faker $faker) {
    return [
        'material' => $faker->material,
        'pin'      => $faker->pin,
        'ram'      => $faker->ram,
        'cpu'      => $faker->cpu,
        'screen'   => $faker->screen,
        'operating_system' => $faker->operating_system,
        'image'   => $faker->image,
    ];
});

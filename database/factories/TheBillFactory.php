<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\admin;
use App\Models\Product;
use App\Models\the_bill;
use Faker\Generator as Faker;

$factory->define(the_bill::class, function (Faker $faker) {
    return [
        'admin_id' => function(){
            return admin::all()->random();
        },
        'product_id' => function(){
            return Product::all()->random();
        }
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product_Type;
use Faker\Generator as Faker;

$factory->define(Product_Type::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});

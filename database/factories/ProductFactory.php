<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use App\Models\Product_Detail;
use App\Models\Product_Type;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'            => $faker->name,
        'product-detail_id' => function(){
            return Product_Detail::all()->random();
        },
        'product-type_id' => function(){
            return Product_Type::all()->random();
        },
        'price' => $faker->price,
        'amount' => $faker->amount,

    ];
});

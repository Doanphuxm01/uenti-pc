@extends('Dashboard.layouts.master')
@section('page_title')
    product detail
@endsection
@section('contents')
    <div class="row">
        <div class="col-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Responsive Hover Table</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>stt</th>
                            <th>material</th>
                            <th>pin</th>
                            <th>ram</th>
                            <th>cpu</th>
                            <th>screen</th>
                            <th>operating_system</th>
                            <th>image</th>
                            <th>chức năng</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($proDetail as $key => $value)
                                <tr>
                                    <td>{{$key++}}</td>
                                    <td>{{$value->material}}</td>
                                    <td>{{$value->pin}}</td>
                                    <td>{{$value->ram}}</td>
                                    <td>{{$value->cpu}}</td>
                                    <td>{{$value->screen}}</td>
                                    <td>{{$value->operating_system}}</td>
                                    <td>
                                        <img style="width:100px;border-radius: 4px" src="{{$value->image}}" alt="">
                                    </td>
                                    <td>
                                        <form action="{{route('proDetail.delete',$value->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection

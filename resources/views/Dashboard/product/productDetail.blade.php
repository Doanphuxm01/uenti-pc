@extends('Dashboard.layouts.master')
@section('page_title')
    product detail
@endsection
@section('contents')
                    <div class="container-fluid">
                        <div class="container">
                            <div class="row">
                                <div class="card w-100 mt-4">
                                    <div class="col-md-12">
                                        <div class="card-header">
                                            <div class="card-title text-md-center">Thêm chi tiết sản phẩm máy tính</div>
                                        </div>
                                        <div class="card-body">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul class="list-group mt-2">
                                                        @foreach ($errors->all() as $error)
                                                            <p>{{ $error }}</p>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                                @if(session()->has('success'))
                                                    <div class="alert alert-success">
                                                        {{ session()->get('success') }}
                                                    </div>
                                                @endif
                                            <form method="post" action="{{route('proDetail.add')}}">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">material </label>
                                                    <input name="material" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">pin </label>
                                                    <input name="pin" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">ram </label>
                                                    <input name="ram" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">cpu </label>
                                                    <input name="cpu" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">screen </label>
                                                    <input name="screen" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">operating_system </label>
                                                    <input name="operating_system" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">image</label>
                                                    <input name="image" type="text" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Nhập tên danh mục" name="cate_name">
                                                </div>
                                                <button type="submit" class="btn btn-primary">Thêm</button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
@endsection

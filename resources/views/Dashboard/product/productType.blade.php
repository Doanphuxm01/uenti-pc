@extends('Dashboard.layouts.master')
@section('page_title')
    create code
@endsection
@section('contents')
    <div class="container-fluid">
        <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                <div class="card-header">
                    <h3 class="card-title">thêm hãng máy tính </h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{route('proType.admin')}}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">tên hãng máy tính </label>
                            <input name="name" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 style="text-align: center" class="card-title">Danh sách hãng máy tính </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">stt</th>
                            <th>name</th>
                            <th>ngày tạo</th>
                            <th style="width: 40px">chức năng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($proType as $key => $value)
                            <tr>
                                <td>{{$key++}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->created_at->format('Y-m-d')}}</td>
                                <td>
                                    <form action="{{route('proType.destroy',$value->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $proType->links() !!}
                </div>
                <!-- /.card-body -->

            </div>
        </div>
    </div>
@endsection


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Registration Page</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="admin/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="admin/plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

    <form action="{{route('admin.postLogin')}}" method="post">
      @csrf
        <div class="form-group has-feedback">
          <input type="text"  name="name" id="name" class="form-control" placeholder="name" >
          <span class="fa fa-user form-control-feedback"></span>
          <span id="error" class="error mt-2 d-lg-block w-100"style="font-size: 14px; margin-left: 18px; color: #ff7675!important;  float: left;"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="email" name="email" id="email" class="form-control" placeholder="email" >
          <span class="fa fa-envelope form-control-feedback"></span>
          <span id="error2" class="error mt-2 d-lg-block w-100"style="font-size: 14px; margin-left: 18px; color: #ff7675!important; float: left;"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="Password">
          <span class="fa fa-lock form-control-feedback"></span>
          <span id="error3" class="error mt-2 d-lg-block w-100"style="font-size: 14px; margin-left: 18px; color: #ff7675!important;  float: left;"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="number"  name="phone_number" id="phone_number" class="form-control" placeholder="phone_number" >
            <span class="fa fa-user form-control-feedback"></span>
            <span id="error" class="error mt-2 d-lg-block w-100"style="font-size: 14px; margin-left: 18px; color: #ff7675!important;  float: left;"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text"  name="address"  class="form-control" placeholder="address" >
            <span class="fa fa-user form-control-feedback"></span>
            <span id="error" class="error mt-2 d-lg-block w-100"style="font-size: 14px; margin-left: 18px; color: #ff7675!important;  float: left;"></span>
        </div>
        <div class="form-group">
            <label>chon quyen</label>
            <select  name="role_id" class="select2 select2-container select2-container--bootstrap4 select2-container--above select2-container--focus">
            @foreach($roles as $key => $value)
                <option  value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
            </select>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button id="save" type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fa fa-facebook mr-2"></i>
          Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fa fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div>

      <a href="login.html" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="admin/plugins/iCheck/icheck.min.js"></script>
</body>
</html>

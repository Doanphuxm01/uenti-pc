$(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass   : 'iradio_square-blue',
          increaseArea : '20%' // optional
        })
      })
      
 $( document ).ready(function() {
    $('#save').on('click',function(){
        event.preventDefault();
            $('#error').hide();
            $('#error2').hide();
            $('#error3').hide();
          var name = $('#name').val();
          var email = $('#email').val();
          var password = $('#password').val();
          $.ajax({
              type: 'POST',
              dataType: 'json',
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data : {
                name : name,
                email : email,
                password : password,
              },
              url:'/register',
              success: function(res){
                alert('register success');
              },
              error: function (res) {
                    let error = $.parseJSON(res.responseText);
                    $('#error').show();
                    $('#error2').show();
                    $('#error3').show();
                    $('#error').text(error.name);
                    $('#error2').text(error.email);
                    $('#error3').text(error.password);
              }
          })
    })
});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function product_type(){
        return $this->BelongsTo(Product_Type::class);
    }
     public function product_detail(){
        return $this->Belongsto(Product_Detail::class);
     }
}

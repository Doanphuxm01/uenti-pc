<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class The_bills extends Model
{
    protected $guarded = [];

    public function admin(){
        return $this->belongsTo(admin::class);
    }

    public function product(){
        return $this->hasMany(Product::class);
    }
}

<?php

namespace App\Http\Controllers\Backend\Admins;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Admins\CreateAdminRequest;
use App\Models\Admins\Admin;
// use App\Repositories\Backend\Admin\AdminContract;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
   public function homeAdmin(){
       $view = view('Dashboard.layouts.master');
       return $view;
   }
}

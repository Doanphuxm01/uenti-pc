<?php

namespace App\Http\Controllers\Backend\Admins;

use App\Models\Product_Detail;
use App\Models\Product_Type;
use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller;

class product_DetailController extends Controller
{
    public function store(Request $request){
        if($request->isMethod('post')){
            $request->validate([
                'material'=>'required',
                'pin'=>'required',
                'ram'=>'required',
                'cpu'=>'required',
                'screen'=>'required',
                'operating_system'=>'required',
                'image'=>'required',
            ]);
            Product_Detail::create($request->all());
            session()->flash('success', 'thêm thành công ');
            return redirect()->route('proDetail.add');
        }
        $proDetail = Product_Detail::latest()->paginate(5);
        $view = view('Dashboard.product.productDetail',['proType' => $proDetail]);
        return $view;
    }
    public function view(){
        $proDetail = Product_Detail::latest()->paginate(5);
        $view = view('Dashboard.product.ProductView',['proDetail' => $proDetail]);
        return $view;
    }
    public function delete($id){
        Product_Detail::findOrFail($id)->delete();
        session()->flash('success', 'xoá thành công ');
        return redirect('/proView');
    }
}

<?php

namespace App\Http\Controllers\Backend\Admins;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use App\Repositories\Backend\Role\RoleContract;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use Validator;
use Illuminate\Support\MessageBag;
use App\Models\admin;
use App\Models\roles;
class LoginController extends Controller
{
    public function getlogin(Request $request)
    {
        return view('Dashboard.login.login');
    }

     public function postLogin(Request $request)
     {
             $email = $request->input('email');
             $password = $request->input('password');
             return redirect()->route('home.admin');
     }
    public function getRegistration()
    {
        $roles = roles::all();
        $view = view('Dashboard.login.register',['roles'=>$roles]);
        return $view;
    }

    public function postRegistration(Request $request){
        $admin = [
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'role_id' => $request->role_id,
            'password' => bcrypt($request->password),
        ];
        $registration = admin::insert($admin);
        return redirect()->route('admin.login');
    }
}

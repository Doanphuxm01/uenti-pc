<?php

namespace App\Http\Controllers\Backend\Admins;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use App\Models\Product_Type;
use Illuminate\Routing\Controller;

class Product_TypeController extends Controller
{
    public function index(Request $request){
       if($request->isMethod('post')){
           $request->validate([
               'name'=>'required',
           ]);
           Product_Type::create($request->all());
           session()->flash('success', 'thêm thành công ');
           return redirect()->route('proType.admin');
       }
       $proType = Product_Type::latest()->paginate(5);
       $view = view('Dashboard.product.productType',['proType' => $proType]);
       return $view;
    }
    public function destroy($id){
        Product_Type::findOrFail($id)->delete();
        session()->flash('success', 'xoá thành công ');
        return redirect()->route('proType.admin');
    }
}

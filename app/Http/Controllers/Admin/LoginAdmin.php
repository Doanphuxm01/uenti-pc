<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\Http\Requests\LoginUser;
class LoginAdmin extends Controller
{
    // use AuthenticatesUsers;

    // protected $redirectTo = '/user';

    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    // protected function guard(){
    //     return Auth::guard('user');
    // }

    public function Login(Request $request){
        if($request->isMethod('Post')){
            dd($request->all());
            $credentials = $request->only('email', 'password');
            $credentials['status'] = 1;
            if(Auth::guard('user')->attempt($credentials)){
                return redirect()->route('admin.home');
            }else{
                return redirect()->back();
            }
        }else{
            $view =view('Dashboard.login.login');
            return $view;
        }
    }
    public function register(LoginUser $request){
        dd('sss');
        if($request->isMethod('Post')){
            $user = [
                'name' =>  $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ];
            $registration = User::insert($user);
            return response()->json([$registration,'success'=>'thêm tài khoản thành công']);
        }
        $view =view('Dashboard.login.register');
        return $view;
    }

    public function logout(Request $request){
        Auth::guard('user')->logout();
        $request->session()->invalidate();
        return redirect()->route('admin.login');
    }
}
